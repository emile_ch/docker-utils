#!/bin/bash

_title(){
  echo -e "\e[1;34m""$*""\e[0;39m"
}
_command(){
  echo -e "\e[1;39m""$*""\e[0;39m"
}
_title2(){
  echo -e "\e[1;95m""$*""\e[0;39m"
}

_color(){
  echo -e "\e[${1}m""$2""\e[0;39m" $3
}

usage(){
  _title2 "Docker Utils"
  echo -e "\e[1;4mCreated with love by emile_ch""\e[0;39m   info: https://gitlab.com/emile_ch/docker_utils"
  printf "\n"
while IFS= read -r line
do
  if [ "${line: -1}" == ")" ] && [[ "${line}" !=  *"#h#"* ]] ; then
  line1=${line::-1}
  Command=${line1/  /}
  elif [[ "${line}" == *"#o#"* ]] ; then
    options="${line/*#o# /}"
  elif [[ "${line}" == *"#h#"* ]] ; then
    comment="${line:8}"
    _command "${comment}"
   _color "${NC}" "./run.sh ${Command}" "$options"
   options=""
  elif [ "${line: -1}" == ")" ] && [[ "${line}" !=  *"#h#"* ]] ; then
  line1=${line::-1}
  Command=${line1/  /}
  elif [[ "${line}" == *"####"* ]]; then
    NC="${line: -2}"
    _title "${line:5:-2}"
  fi
done < run.sh
}