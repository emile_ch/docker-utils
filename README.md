# Docker utilities

Some docker utilities, easily accessible via a web browser and easy to launch.

## Services

##### Traefik
[Traefik](https://containo.us/traefik/) is a router / reverse proxy very useful and easy to use with docker.  
In this configuration, traefik exposes http and https ports and forwards traffic to containers.  
Hostname for a container is specified with labels. 
If you use Linux or MacOs, you can use hostname.localhost and acces the web ui of a container without any changes in your */etc/hosts* file.  
Traefik itself is configured to use traefik, you can access traefikweb ui at http://traefik.localhost

##### Portainer 
[Portainer](https://www.portainer.io/) is a docker management system with a Web UI.  
Access to the interface in http://portainer.localhost

##### Doozle
[Dozzle](https://github.com/amir20/dozzle) is a real time log viewer for docker.  
Access it: http://dozzle.localhost

## How to launch the stack

Stack launching and others actions are managed by the *run.sh* script.  
`./run.sh launch` will create all necessary docker network and volumes and launch the stack.

To see others possible actions, run `/run.sh help`


## DNS resolution

With traefik, you can access to container's web ui with friendly hostname (instead of container's IP or port forwarding).
In order to use hostname, you got to resolve every *.localhost to 127.0.0.1.
There are to ways to do that:
* Use a DNS wildcard (see below)
* Resolve every hostname in your hosts file (**not recommended**  [example here](https://www.howtogeek.com/howto/27350/beginner-geek-how-to-edit-your-hosts-file/) )


#### Linux / MacOs with Chrome or Chromium:
If you use Linux or MacOs with chrome or chromium, you can access every [host].localhost without others modifications.  

#### Linux and Firefox or others browsers:
Firefox does not provide auto resolution for localhost, so you'll have to use dnsmasq with the following config:  
`address=/.localhost/127.0.0.1`

#### Windows: 
For windows, you can use [Acrylic proxy](http://mayakron.altervista.org/wikibase/show.php?id=AcrylicHome)

#### Use another local domain

If you want to use another local domain, change 'local' in .env and ensure this domain is resolved to 127.0.0.1  
NB: even if you use Linux or MacOs and chrome, you will need to set a DNS wildcard to your new domain with dnsmasq. 


## Configuration
You can change configuration with the .env file or by changing the *docker-utils.yml* file

### Use traefik for others containers:
Add the container in the network defined in *.env* file with the key *networkFrontend*
Add necessary labels to your container, as it is done with containers in *docker-utils.yml*

## Useful aliases
Copy paste the following lines in your *bashrc* file or *aliases* file:
```
alias run="./run.sh"
```
You can now use `run` instead of `./run.sh`  
Of course, you can replace `run` by a preferred alias name.