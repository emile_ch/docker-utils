#!/bin/bash

source .env
source ./doc/usage.sh
set -e # exit on error

case $1 in
  network)
    #h# create the network ${networkFrontend}
    echo "creating the network: ${networkFrontend} "
    docker network create "${networkFrontend}" ;;
  volume)
    #h# create the volume: ${portainerDataVolume}
    echo "creating the volume: ${portainerDataVolume} "
    docker volume create --name="${portainerDataVolume}" ;;
  launch)
    #h# create necessary volumes and networkd and launch the stack
    #o# [docker-compose options]
    shift
    ./run.sh network
    ./run.sh volume
    docker-compose -p utils -f ./docker-utils.yml up -d $* ;;
  up)
    #h# Create or recreate the stack
    shift
    docker-compose -p utils -f ./docker-utils.yml up -d $* ;;
  stop)
    #h# Stop all the containers or the containers listed as args
    #o# [container's names]
    shift
    docker-compose -f ./docker-utils.yml -p utils stop $* ;;
  start)
    #h# Start all the containers or the containers listed as args
    #o# [container's names]
    shift
    docker-compose -f ./docker-utils.yml -p utils start $* ;;
  down)
    #h# Stop and remove all the containers
    docker-compose -p utils -f ./docker-utils.yml down;;
  debug)
    #h# Launch the stack and display logs
    shift
    docker-compose -p utils -f ./docker-utils.yml up $* ;;
  config)
    #h# Display config (usign the .env file)
    docker-compose -f ./docker-utils.yml config ;;
  usage|help|-h|h) usage ;;
  *) _color 31 "The super ./run.sh cannot understand what you said, sorry :( "
    echo "see './run.sh help' "
esac

